#include <stdio.h>
#include <conio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
//void bubbleSort(double a[],int n);


void main() {
	int width,height;
	int i,j,k;
	int choice=0; FILE *praw,*fc; unsigned char *image;
	char filename[50],savename[50];
	
	printf("請輸入欲處理的相片檔名:\n");
	scanf("%s",&filename);
	printf("請輸入相片寬度:\n");
	scanf("%d",&width);
	printf("請輸入相片長度:\n");
	scanf("%d",&height);
	
	praw=fopen(filename,"rb");
	image=new unsigned char[width*height*3];
	fread(image,sizeof(unsigned char),width*height*3,praw);
	
	do {
		printf("1.輸出原圖\n");
		printf("2.加入雜訊\n");
		printf("3.裁切成一張8M*8N的大小\n");
		printf("4.不同像元大小的數位彩色影像\n");
		printf("5.改變dn等級\n");
		printf("6.亮度影像\n");
		printf("7.RGB波段互換\n");
		printf("8.中值濾波\n");
		printf("請輸入處理函式：");
		scanf("%d",&choice);
		switch(choice){
		case 1:{
			unsigned char*outimage;
			outimage=new unsigned char[width*height*3];
			
			for(i=0;i<width*height*3;i++)
				outimage[i]=image[i];
			printf("請輸入輸出名稱:\n");
			scanf("%s",&savename);
			
			fc=fopen(savename,"wb");
			fwrite(outimage,sizeof(unsigned char),(width*height)*3,fc);
			fclose(praw);
			fclose(fc);
			break;}
		case 2:{
			unsigned char *newimage;
			newimage=new unsigned char[width*height*3];
			FILE *text;
			int a;
			double dn;
			text=fopen("data.txt","w");
			
			for(i=0;i<width*height*3;i++){
				double sum=0;
				for(a=0;a<12;a++)
				sum=sum+rand()/(double)RAND_MAX;
				dn=30*(sum-6.0);
				if(image[i]+dn>255){
					dn=255-image[i];
					image[i]=255;}
				else if(image[i]+dn<0){
					dn=0-image[i];
					image[i]=0;}
				else
					image[i]=image[i]+dn;
				fprintf(text,"%.2f\n",dn);
			}
			printf("請輸入輸出名稱:\n");
			scanf("%s",&savename);
			fc=fopen(savename,"wb");
			fwrite(image,sizeof(unsigned char),(width*height)*3,fc);
			fclose(praw);
			fclose(fc);
			break;}
		case 3:{
			unsigned char*cutimage;
			int w,h,k;
			w=width%8;
			h=height%8;
			int r=0;
			int g=(width-w)*(height-h);
			int b=(width-w)*(height-h)*2;
			cutimage=new unsigned char[(width-w)*(height-h)*3];
			
			if(w==0&&h==0)
				printf("已符合8M*8N條件");
			else {
				for (i=0;i<(height-h);i++){
					for (j=0;j<(width-w);j++){
						k=j+width*i;
						cutimage[r]=image[k];
						r++;
					}
				}
				for (i=height;i<(height*2-h);i++){
					for (j=0;j<(width-w);j++){
						k=j+width*i;
						cutimage[g]=image[k];
						g++;
					}
				}
				for (i=height*2;i<(height*3-h);i++){
					for (j=0;j<(width-w);j++){
						k=j+width*i;
						cutimage[b]=image[k];
						b++;
					}
				}
			}
			printf("請輸入輸出名稱:\n");
			scanf("%s",&savename);
			fc=fopen(savename,"wb");
			fwrite(cutimage,sizeof(unsigned char),((width-w)*(height-h))*3,fc);
			fclose(praw);
			fclose(fc);
			break;}
		case 4:{
			printf("請執行圖片縮小尺寸項目程式，執行的次數為下列項目(1)4M*4N(2)2M*2N(3)M*N\n");
			unsigned char *pic;
			pic=new unsigned char[width*height*3/4];
			int a=0;
			int value;
			
			for(i=0;i<(width*height)*3;i=i+2){
				if(i/width%2==0){
					for(j=0;j<2;j++){
						for(k=0;k<2;k++){
							value=value+image[i+width*j+k];
						}
					}
					pic[a]=value/4;
					a++;
					value=0;
				}
			}
			printf("請輸入輸出名稱:\n");
			scanf("%s",&savename);
			fc=fopen(savename,"wb");
			fwrite(pic,sizeof(unsigned char),(width*height)*3/4,fc);
			fclose(praw);
			fclose(fc);
			break;}
		case 5:{
			int dn,t;
			
			printf("請輸入色階數(64 or 8 or 2):\n");
			scanf("%d",&dn);
			if(dn!=2&&dn!=8&&dn!=64){
				printf("請重新輸入色階數(64 or 8 or 2):\n");
				scanf("%d",&dn);
			}
			
			t=256/dn;
			unsigned char *pic;
			pic=new unsigned char[width*height*3];
			
			for(i=0;i<width*height*3;i++)
				pic[i]=image[i]/t*t;
			printf("請輸入輸出名稱:\n");
			scanf("%s",&savename);
			fc=fopen(savename,"wb");
			fwrite(pic,sizeof(unsigned char),(width*height)*3,fc);
			fclose(praw);
			fclose(fc);
			break;}
		case 6:{
			int temp;
			unsigned char *intensity;
			intensity=new unsigned char[width*height*3];
			
			for(i=0;i<width*height;i++){
				temp=(image[i]+image[i+width*height]+image[i+2*width*height])/sqrt(3.0);
				if(temp>=255)
					intensity[i]=255;
				else
					intensity[i]=temp;
			}
			printf("請輸入輸出名稱:\n");
			scanf("%s",&savename);
			fc=fopen(savename,"wb");
			fwrite(intensity,sizeof(unsigned char),(width*height)*3,fc);
			fclose(praw);
			fclose(fc);
			break;}
		case 7:{
			int k;
			unsigned char *img;
			img=new unsigned char[width*height*3];
			int e,ee,eee;
			
			printf("請依序輸入相對應欲換取的波段,(1)R(2)G(3)B\n");
			printf("R->:");
			scanf("%d",&e);
			if(e==1){
				for(k=0;k<width*height;k++){img[k]=image[k];}
			}
			if(e==2){
				for(k=0;k<width*height;k++){img[k]=image[k+width*height];}
			}
			if(e==3){
				for(k=0;k<width*height;k++){img[k]=image[k+2*width*height];}
			}
			printf("G->:");
			scanf("%d",&ee);
			
			if(ee==1){
				for(k=0;k<width*height;k++){img[k+width*height]=image[k];}
			}
			if(ee==2){
				for(k=0;k<width*height;k++){img[k+width*height]=image[k+width*height];}
			}
			if(ee==3){
				for(k=0;k<width*height;k++){img[k+width*height]=image[k+2*width*height];}
			}
			
			printf("B->:");
			scanf("%d",&eee);
			if(eee==1){
				for(k=0;k<width*height;k++){img[k+2*width*height]=image[k];}
			}
			if(eee==2){
				for(k=0;k<width*height;k++){img[k+2*width*height]=image[k+width*height];}
			}
			if(eee==3){
				for(k=0;k<width*height;k++){img[k+2*width*height]=image[k+2*width*height];}
			}
			printf("請輸入輸出名稱:\n");
			scanf("%s",&savename);
			fc=fopen(savename,"wb");
			fwrite(img,sizeof(unsigned char),(width*height)*3,fc);
			fclose(praw);
			fclose(fc);
			break;}
		case 8:{
			break;}

		default:
		printf("請重新輸入\n");
		break;}
	}while(choice > 8 || choice < 1);
	
	system("pause");
	

}
